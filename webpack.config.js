const path = require('path');
const webpack = require('webpack');

module.exports = {
    entry: './web/index.js',
    output: {
        path: path.resolve(__dirname, 'public', 'dist'),
        filename: 'app.min.js'
    },
    module: {
        loaders: [
            { 
              test: /\.js$/, 
              loader: 'babel-loader', 
              query: {
                presets: ['es2015'],
                plugins: ['transform-es2015-destructuring', 'transform-object-rest-spread']
              }, 
              exclude: /node_modules/ },
            { test: /\.html$/, loader: 'raw-loader' }, // npm install --save-dev raw-loader
             { 
                test: /\.vue$/, 
                loader: 'vue-loader'
                , options: {
                    loaders: {
                        scss: 'vue-style-loader!css-loader!sass-loader' // <style lang="sass">
                    }
                }
            }

        ]
    },
    stats: {
        colors: true
    },
    plugins: [
        // new webpack.optimize.UglifyJsPlugin({
        //     compress: {
        //         warnings: false,
        //         drop_console: false,
        //     }
        // })
    ],
    resolve: {
        alias: {
            vue: 'vue/dist/vue.js'
        }
    },
    devtool: 'source-map'
}
