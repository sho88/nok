# NokNok

## The concept

## Getting running

1. Create an environment file: `.env` like this:

```
NODE_ENV=development
DATABASE_HOST=db
MYSQL_ROOT_PASSWORD=noknok  
MYSQL_DATABASE=noknok  
MYSQL_USER=noknok
MYSQL_PASSWORD=whosthere
MYSQL_PORT=3306
SERVER_PORT=8000
SESSION_SECRET=nice long random phrase
```

2. `npm install`

3. Run `npm run start:express` and `npm run start:vue` in separate commands

4. If editing the SCSS `gulp css:watch`

5. NokNok should be running at [http://127.0.0.1:8000]

6. To sign in- username `test@email.com`, password: empty.

## Getting running with Docker

1. `docker-compose build`
2. `docker-compose up`

## Library documentation

### API
- [Express - Web Framework](http://expressjs.com/)
- [Sequelize - Database](https://docs.sequelizejs.com)
- [Passport - Authentication](http://www.passportjs.org/docs/)

### Frontend
- [Vue](https://vuejs.org/)
- [Axios - HTTP Client](https://github.com/axios/axios)
- [DayJS - Date Handling](https://github.com/iamkun/dayjs)
