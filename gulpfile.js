const { src, dest, watch, task, series } = require('gulp');

const sass = require('gulp-sass');
const minifyCSS = require('gulp-csso');
const concat = require('gulp-concat');

task('css', function(){
  return src('client/stylesheets/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(minifyCSS())
    .pipe(concat('app.min.css'))
    .pipe(dest('public/dist'))
});

task('css:watch', function () {
  watch(
    'client/stylesheets/*.scss',
    { ignoreInitial: false },
    series('css')
  );
});

task('default', series('css'));
