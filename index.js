const bParser   = require('body-parser');
const env       = require('dotenv');
const express   = require('express');
const helmet    = require('helmet');
const passport  = require('passport');
const session   = require('express-session');
const models    = require('./api/models');

env.config();

const PORT = process.env.SERVER_PORT;
const SESSIONSECRET = process.env.SESSION_SECRET;

const app = express();

app.use(bParser.urlencoded({ extended: true }));
app.use(bParser.json());

// Passport
app.use(session({
    secret: SESSIONSECRET,
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());

app.use(helmet());
app.use(express.static('public'));

const apiRoutes = require('./api')(app, passport);

require('./api/config/passport.js')(passport, models.user);

// Sync the database
models.sequelize.sync() //({force: true}) // Warning: FORCE will nuke the database
  .then(() => {
    console.log('Database in sync');
  })
  .catch((err) => {
    console.log(err, 'Database sync error');
  });

// start app / listen on ports
app.listen(PORT, () => console.log( 'Running on port: ' + PORT ));
