import Vue from 'vue';
import Router from 'vue-router';

import auth from '../middleware/auth';

import DashboardComponent from '../components/Dashboard.vue';
import UserComponent from '../components/User.vue';
import HomeComponent from '../components/Home.vue';
import StreetComponent from '../components/Street.vue';
import SettingsComponent from '../components/settings/Settings.vue';
import ChurchSettingsComponent from '../components/settings/ChurchSettings.vue';
import UserSettingsComponent from '../components/settings/UserSettings.vue';
import VisitForm from '../components/VisitForm.vue';
import ResidenceComponent from '../components/Residence.vue';

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '/',
      component: HomeComponent
    },
    {
      path: '/dashboard',
      component: DashboardComponent,
      meta: { middleware: auth },
    },
    {
      path: '/user/new',
      component: UserComponent,
      meta: { middleware: auth },
    },
    {
      path: '/street/:street_id',
      component: StreetComponent,
      meta: { middleware: auth },
    },
    {
      path: '/street/:street_id/:residence_id/new-visit',
      component: VisitForm,
      meta: { middleware: auth },
    },
    { 
      path: '/settings', 
      component: SettingsComponent,
      meta: { middleware: auth },
    },
    { 
      path: '/settings/church', 
      component: ChurchSettingsComponent,
      meta: { middleware: auth },
    },
    {
      path: '/street/:street_id/:residence_id/',
      component: ResidenceComponent,
      meta: { middleware: auth },
    },
    { 
      path: '/settings/user', 
      component: UserSettingsComponent,
      meta: { middleware: auth },
    },
  ]
});

function nextFactory(context, middleware, index) {
  const subsequentMiddleware = middleware[index];
  if (!subsequentMiddleware) return context.next;

  return (...parameters) => {
    context.next(...parameters);
    const nextMiddleware = nextFactory(context, middleware, index + 1);
    subsequentMiddleware({ ...context, next: nextMiddleware });
  };
}

router.beforeEach((to, from, next) => {
  if (to.meta.middleware) {
    const middleware = Array.isArray(to.meta.middleware)
      ? to.meta.middleware
      : [to.meta.middleware];
    
    const context = {from, next, router, to};
    const nextMiddleware = nextFactory(context, middleware, 1);
    return middleware[0]({ ...context, next: nextMiddleware});
  }
  return next();
});

export default router;
