import dayjs from 'dayjs';

// Check 12 months isn't up…
function knockFlagValid(dnkFlagDate) {
  const lastYear = dayjs().subtract(1, 'year');
  const flagSetDate = dayjs(dnkFlagDate);
  
  if (lastYear > flagSetDate) { return false; }
  return true;
}


// Returns true or false depending on if the do_not_knock flag is valid
export const canKnock = residenceData => {
  const {do_not_knock, do_not_knock_date} = residenceData;
  
  if (do_not_knock && knockFlagValid(do_not_knock_date)) {
    return false;
  }
  return true;

};


