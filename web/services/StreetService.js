import axios from 'axios'

export const add = data => {
	return axios.post('/mongo', data);
};

export const removeById = id => {
	return axios.delete(`/streets/${id}/delete`);
};

export const addProperty = data => {
	return axios.post(`/properties`, data);
};

export const editProperty = data => {
	return axios.put(`/properties/${data.id}`, data);
};