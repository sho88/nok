import { isUndefined } from 'lodash';

export const AppMixin = {
	
	created() {
	},

	data() {
	},

	computed: {
		auth: {
      get: function() { 
        const uInfo = this.getStorage('user');
        if (!uInfo) {
          this.endSession('You must be logged in');
          return;
        }
        return uInfo;
      },
      set: function(userData) {
        this.setStorage('user', userData);
      }
    }
	},

	methods: {
    endSession(msg) {
      this.$http.get(`/api/logout`)
        .then(() => {
          this.deleteStorage('user');
          this.deleteStorage('church');
          if (msg) {
            this.flash(msg, 'warning', {timeout: 5000, important: true });
          }
          this.$router.push('/');
        });
    },
		setStorage(key, data) {
      localStorage.setItem(key, JSON.stringify(data));
    },
    getStorage(key) {
      return JSON.parse(localStorage.getItem(key));
    },
		deleteStorage(key) {
			this.auth = null;
      return localStorage.removeItem(key);
    },
    getChurchId: function() {
      return (isUndefined(this.auth))
      ? null
      : this.auth.church_id;
    },
    userChurch() {
      return this.getStorage('church');
    },
    getAuthenticateName() {
			return (isUndefined(this.auth))
				? null
				: `${this.auth.forename} ${this.auth.surname}`;
    },
    getAuthenticateId() {
			return (isUndefined(this.auth))
				? null
				: this.auth.id;
    }
	}

};
