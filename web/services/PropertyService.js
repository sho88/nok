import axios from 'axios';

export const getPropertiesByStreetId = id => {
  return axios.get(`/streets/properties/${id}`);
};
