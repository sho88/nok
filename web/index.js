import Vue from 'vue';
import App from './App.vue';
import VueCookies from 'vue-cookies';
import Vuex from 'vuex';
import VueFlashMessage from 'vue-flash-message';
import Vuelidate from 'vuelidate';
import axios from 'axios';
import * as lodash from 'lodash';
import dayjs from 'dayjs';
import relativeTime from 'dayjs/plugin/relativeTime'
import slugify from 'slugify';
import router from './config/routes';
import { state, mutations } from './config/state';

dayjs.extend(relativeTime);

// let Vue know that it's going to use Vuelidate, VueRouter, Vuex etc
Vue.use(Vuelidate);
Vue.use(Vuex);
Vue.use(VueFlashMessage);
Vue.use(VueCookies);

// Register the $http to Axios
Vue.prototype.$http = axios;
Vue.prototype.$lodash = lodash;
Vue.prototype.$dayjs = dayjs;
Vue.prototype.$slugify = slugify;

// Register the store and router (to be inserted into Vue)
const store = new Vuex.Store({ state, mutations })

Vue.config.productionTip = false;

// Initialize the app
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
