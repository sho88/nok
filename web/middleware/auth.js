export default function auth({ next, router }) {

  if (!localStorage.getItem('user')) {
    console.log('You are not logged in');
    return router.push('/');
  }
  
  return next();
}
