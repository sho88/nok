const AppController = require('./AppController');
const httpStatusCodes = require('http-status-codes');

class VisitsController extends AppController {

  constructor({ VisitsService }) {
    super();
    this.VisitsService = VisitsService;

    this.indexAction = this.indexAction.bind(this);
  }

  indexAction(req, res) {
    const residence_id = req.headers.residence_id;

    if (!residence_id) {
      return this.ResponseService.parse(
        res,
        { err: 'Residence ID required' },
        httpStatusCodes.BAD_REQUEST
      );
    }

    if (req.method === 'GET') {
      return this.VisitsService.getVisitsByResidenceId(residence_id)
        .then(visits => this.ResponseService.parse(res, visits, httpStatusCodes.CREATED))
        .catch(err => this.ResponseService.parse(res, { err: err.message }, httpStatusCodes.BAD_REQUEST));
    }

    if (req.method === 'POST') {
      return this.VisitsService.createNewVisit(residence_id, req.body)
        .then(visits => this.ResponseService.parse(res, visits, httpStatusCodes.CREATED))
        .catch(err => this.ResponseService.parse(res, { err: err.message }, httpStatusCodes.BAD_REQUEST));
    }
  }


}

module.exports = VisitsController;

