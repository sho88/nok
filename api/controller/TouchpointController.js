const AppController = require('./AppController');
const httpStatusCodes = require('http-status-codes');

class TouchpointController extends AppController { 

  constructor({ TouchpointService }) { 
    super();
    this.TouchpointService = TouchpointService;

    this.indexAction = this.indexAction.bind(this);
    this.createAction = this.createAction.bind(this);
  }

  indexAction(req, res) {
    if (req.method === 'GET') {

      if (!req.headers.church_id) {
        return this.ResponseService.parse(res, { err: 'Church ID required' }, httpStatusCodes.BAD_REQUEST)
      }

      return this.TouchpointService.getTouchpointsByChurchId(req.headers.church_id)
        .then(street => this.ResponseService.parse(res, street, httpStatusCodes.CREATED))
        .catch(err => this.ResponseService.parse(res, { err: err.message }, httpStatusCodes.BAD_REQUEST));
    }
  }

  createAction(req, res) {
    if (req.method === 'POST') {

      const churchId = req.headers.church_id;

      if (!churchId) {
        return this.ResponseService.parse(res, { err: 'Church ID required' }, httpStatusCodes.BAD_REQUEST)
      }
      
      return this.TouchpointService.createTouchpoint(churchId, req.body)
        .then(touchpoint => this.ResponseService.parse(res, touchpoint, httpStatusCodes.CREATED))
        .catch(err => this.ResponseService.parse(res, { err: err.message }, httpStatusCodes.BAD_REQUEST));
    }
  }
  
}

module.exports = TouchpointController;
