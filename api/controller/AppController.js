const ResponseService = require('./../service/ResponseService');

class AppController {
  // there must be a way that we can effectively add some dependency injection
  constructor() {
    this.ResponseService = new ResponseService;
  }
}

// returns a single instance of itself
module.exports = AppController;