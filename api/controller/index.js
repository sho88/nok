const IndexController = require('./IndexController');
const ResidenceController = require('./ResidenceController');
const StreetController = require('./StreetController');
const TouchpointController = require('./TouchpointController');
const ChurchController = require('./ChurchController');
const VisitsController = require('./VisitsController');

const ResidenceService = require('./../service/ResidenceService');
const StreetService = require('./../service/StreetService');
const TouchpointService = require('./../service/TouchpointService');
const UserService = require('./../service/UserService');
const VisitsService = require('./../service/VisitsService');
const ChurchService = require('./../service/ChurchService');

module.exports = {
  IndexController: new IndexController({ UserService: new UserService() }),
  
  ResidenceController: new ResidenceController({
    ResidenceService: new ResidenceService(),
    StreetService: new StreetService(),
    VisitsService: new VisitsService()
  }),
  
  StreetController: new StreetController({ StreetService: new StreetService() }),
  VisitsController: new VisitsController({ VisitsService: new VisitsService() }),
  TouchpointController: new TouchpointController({ TouchpointService: new TouchpointService() }),
  ChurchController: new ChurchController({ ChurchService: new ChurchService() })
};
