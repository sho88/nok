const AppController = require('./AppController');
const httpStatusCodes = require('http-status-codes');

class StreetController extends AppController {
  constructor({ StreetService }) {
    super();
    this.StreetService = StreetService;
    this.indexAction = this.indexAction.bind(this);
    this.viewAction = this.viewAction.bind(this);
  }

  indexAction(req, res) {
    const church_id = req.headers.church_id;

    if (!church_id) {
      return this.ResponseService.parse(res, { err: 'Church ID required' }, httpStatusCodes.BAD_REQUEST)
    }
    
    if (req.method === 'POST') {
      return this.StreetService.createStreet(church_id, req.body)
        .then((street) => {
          console.log(street);
          this.ResponseService.parse(res, street, httpStatusCodes.CREATED)
        })
        .catch(err => this.ResponseService.parse(res, { err: err.message }, httpStatusCodes.BAD_REQUEST));
    }

    if (req.method === 'GET') {
      return this.StreetService.getStreets(church_id)
        .then(streetModels => this.ResponseService.parse(res, streetModels))
        .catch(err => this.ResponseService.parse(res, { err: err.message }, httpStatusCodes.BAD_REQUEST));
    }
  }

  viewAction(req, res) {
    return this.StreetService.getStreetById(req.params.id)
      .then(streetModel => this.ResponseService.parse(res, streetModel))
      .catch(err => this.ResponseService.parse(res, { err: err.message }, httpStatusCodes.BAD_REQUEST));
  }
}

module.exports = StreetController;
