const AppController = require('./AppController');
const httpStatusCodes = require('http-status-codes');

class ChurchController extends AppController {
  constructor({ ChurchService }) {
    super();
    this.ChurchService = ChurchService;
    this.indexAction = this.indexAction.bind(this);
    this.viewByChurchId = this.viewByChurchId.bind(this);
  }

  indexAction(req, res) {
    if (req.method === 'GET') {
      return this.ChurchService.allChurches()
        .then(church => this.ResponseService.parse(res, church))
        .catch(err => this.ResponseService.parse(res, { err: err.message }, httpStatusCodes.BAD_REQUEST));
    }

    if (req.method === 'POST'){}
    
    if (req.method === 'PATCH'){}
  }

  viewByChurchId(req, res) {
    const church_id = req.params.church_id;

    if (!church_id) {
      return this.ResponseService.parse(
        res,
        { err: 'Church ID required' },
        httpStatusCodes.BAD_REQUEST
      );
    }

    return this.ChurchService.viewByChurchId(church_id)
      .then(church => this.ResponseService.parse(res, church))
      .catch(err => this.ResponseService.parse(res, { err: err.message }, httpStatusCodes.BAD_REQUEST));
  }
}

module.exports = ChurchController;
