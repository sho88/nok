const AppController = require('./AppController');
const moment = require('moment');
const httpStatusCodes = require('http-status-codes');
const passport = require('passport');

class IndexController extends AppController {
  constructor({ UserService }) {
    super();
    this.UserService = UserService;
    this.indexAction = this.indexAction.bind(this);
    this.loginAction = this.loginAction.bind(this);
    this.logoutAction = this.logoutAction.bind(this);
    this.currentUserAction = this.currentUserAction.bind(this);
    this.newUserAction = this.newUserAction.bind(this);
  }

  indexAction(req, res) {
    return this.ResponseService.parse(res, { message: `Welcome to Nok Nok API (c) ${moment().year()}` });
  }

  loginAction(req, res, next){
    passport.authenticate('local-signin', (err, user, info) => {
      if (err) {
        return next(err);
      }

      if (!user) {
        console.log('Cannot sign in:', info.message);
        return this.ResponseService.parse(res, { err: info }, httpStatusCodes.UNAUTHORIZED);
      }

      req.login(user, err => {
        console.log('User signed in')
        res.send("Signed in");
      });
    })(req, res, next);
  }

  logoutAction(req, res) {
    req.logout();
    console.log("User signed out");
    return this.ResponseService.parse(res, { message: 'User signed out' });
  }
  
  currentUserAction(req, res) {
    const user_id = req.session.passport.user;
    if (req.method === 'GET' && user_id) {
      return this.UserService.getUser(user_id)
        .then((user) => {
          this.ResponseService.parse(res, user, httpStatusCodes.OK)
        })
        .catch(err => this.ResponseService.parse(res, { err: err.message }, httpStatusCodes.BAD_REQUEST));
    }
  }

  newUserAction(req, res, next) {
    passport.authenticate('local-signup', (err, user, info) => {
      if (err) { return next(err); }

      if (!user) {
        console.log('Cannot create user:', info.message);
        return this.ResponseService.parse(res, { err: info }, httpStatusCodes.BAD_REQUEST);
      }

      req.login(user, err => {
        console.log('User created successfully');
        res.send("User created");
      });

    })(req, res, next);
  }

}

module.exports = IndexController;
