const AppController = require('./AppController');
const httpStatusCodes = require('http-status-codes');

class ResidenceController extends AppController {

  constructor({ ResidenceService, StreetService, VisitsService }) {
    super();
    this.ResidenceService = ResidenceService;
    this.StreetService = StreetService;
    this.VisitsService = VisitsService;

    this.indexAction = this.indexAction.bind(this);
    this.viewByStreetId = this.viewByStreetId.bind(this);
    this.viewByHouseNumber = this.viewByHouseNumber.bind(this);
    this.viewByResidenceId = this.viewByResidenceId.bind(this);
  }

  indexAction(req, res) {
    // TODO: List
    if (req.method === 'GET') {
      console.log('Nothing to see here…');
    }

    // Create
    if (req.method === 'POST') {
      return this.ResidenceService.createResidence(req.body)
        .then(residence => this.ResponseService.parse(res, residence, httpStatusCodes.CREATED))
        .catch(err => this.ResponseService.parse(res, { err: err.message }, httpStatusCodes.BAD_REQUEST));
    }

    // Update 
    if (req.method === 'PATCH') {
      if (!req.headers.residence_id) {
        return this.ResponseService.parse(
          res,
          { err: 'Residence ID required' },
          httpStatusCodes.BAD_REQUEST
        )
      }

      const residenceId = req.headers.residence_id;

      return this.ResidenceService.updateResidence(residenceId, req.body)
        .then(residence => this.ResponseService.parse(res, residence, httpStatusCodes.CREATED))
        .catch(err => this.ResponseService.parse(res, { err: err.message }, httpStatusCodes.BAD_REQUEST));

    }
  }

  viewByResidenceId(req, res) {
    return this.ResidenceService.getByResidenceId(req.params.residenceId)
      .then(residenceModel => this.ResponseService.parse(res, residenceModel))
      .catch(err => this.ResponseService.parse(res, { err: err.message }, httpStatusCodes.BAD_REQUEST));
  }

  viewByHouseNumber(req, res) {
    // @TODO: Find a way to ensure that you throw an error if parameter isn't a number or something (or slap in some middleware actually)
    return this.ResidenceService
      .getByHouseNumber(req.params.houseNumber)
      .then(residence => {
        const { id, street_id } = residence;
        return this.VisitsService
          .getVisitsByResidenceId( id )
          .then(visits => {
            // ...and get the street they belong to
            return this.StreetService
              .getStreetById( street_id )
              .then(street => this.ResponseService.parse(res, { residence, street, visits }))
          })
      })
      .catch(err => this.ResponseService.parse(res, { err: err.message }, httpStatusCodes.BAD_REQUEST));
  }

  viewByStreetId(req, res) {
    // @TODO: Find a way to ensure that you throw an error if parameter isn't a number or something (or slap in some middleware actually)
    return this.ResidenceService.getByStreetId(req.params.id)
      .then(residences => {
        return this.StreetService
          .getStreetById(req.params.id)
          .then(street => this.ResponseService.parse(res, { street, residences }));
      })
      .catch(err => this.ResponseService.parse(res, { err: err.message }, httpStatusCodes.BAD_REQUEST));
  }

}

module.exports = ResidenceController;
