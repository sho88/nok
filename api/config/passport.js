const bCrypt = require('bcrypt');

module.exports = function(passport, user) {
  var User = user;
  var LocalStrategy = require('passport-local').Strategy;

  passport.use('local-signup', new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'pass',
      passReqToCallback: true,
    },
    (req, email, password, done) => {

      const generateHash = (password) => {
        return bCrypt.hashSync(password, bCrypt.genSaltSync(8), null);
      };

      User.findOne({
        where: {
          email: email
        }
      }).then(function(user) {
        if (user) {
          return done(null, false, {
            message: 'That user already exists',
          });
        } else {
          const userPassword = generateHash(password);
          const data = {
            email: email,
            pass: userPassword,
            forename: req.body.forename,
            surname: req.body.surname,
            church_id: req.body.church_id
          };

          User.create(data).then((newUser, created) => {
            if (!newUser) {
              return done(null, false);
            }
            
            return done(null, newUser);
          });
        }
      });
    }
  ));

  passport.use('local-signin', new LocalStrategy(
    {
      usernameField: 'email',
      passwordField: 'password',
      passReqToCallback: true
    },
    (req, email, password, done) => {
      const User = user;
      const isValidPassword = (userpass, password) => {
        return bCrypt.compareSync(password, userpass);
      }
      User.findOne({
        where: {
          email: email
        }
      }).then((user) => {
        if (!user) {
          return done(null, false, {
            message: 'Email does not exist'
          });
        }
        if (!isValidPassword(user.pass, password)) {
          return done(null, false, {
            message: 'Incorrect password'
          });
        }
        
        const userInfo = user.get();
        return done(null, userInfo);
      }).catch((err) => {
        console.log('Something went wrong');
        console.log(err);

        return done(null, false, {
          message: 'Something went wrong during sign in'
        });
      });
    }
  ));

  passport.serializeUser((user, done) => {
      done(null, user.id);
  });

  passport.deserializeUser((id, done) => {
    console.log('Deserialize');
    User.findByPk(id).then((user) => {
      if (user) { 
        done(null, user.get());
      } else {
        done(user.errors, null);
      }
    })
  });
}

