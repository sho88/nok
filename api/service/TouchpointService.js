const Touchpoint = require('../models').touchpoint;

class TouchpointService {
  getTouchpointsByChurchId(church_id) {

    return Touchpoint.findAll({
      where: { church_id }
    })
    .then(data => {
      return data;
    })
  }

  createTouchpoint(church_id, { touchpoint_name }) {

    return Touchpoint.create({
      church_id, touchpoint_name
    }).then(data => {
      return data.get();
    });
  }
}

module.exports = TouchpointService;
