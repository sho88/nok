const Residence = require('../models').residence;
const Street = require('../models').street;
const Visit = require('../models').visit;
const Sequelize = require("sequelize");

class ResidenceService { 
  getByStreetId( street_id ) {

    return Residence.findAll({
      attributes: [
        'id', 
        'house_number', 
        'street.name', 
        'do_not_knock', 
        'do_not_knock_date', 
        [Sequelize.fn('max', Sequelize.col('visits.createdAt')), 'last_visited'] // Nb: Sequelize trickery with the pluralisation of visits
      ],
      where: {
        street_id
      },
      include: [
        {
          model: Street,
          required: true
        },
        {
          model: Visit,
          required: false
        }
      ],
      group: ['residence.id'],
      order: [['house_number', 'ASC']]
    })
      .then((data) => {
        return data;
      });
  }  
  
  createResidence({ streetId, houseNumber }) {
    return Residence.create({
      street_id: streetId,
      house_number: houseNumber
    }).then(data => {
      return data;
    });
  }

  getByResidenceId(residence_id) {
    return Residence.findOne(
      {
        where: {
          id: residence_id
        },
        include: [
          {
            model: Street,
            required: false
          }
        ]
      }
    )
    .then((data) => {
      return data.get();
    })
  }

  updateResidence(residence_id, residence_fields) {
    return Residence.update(
      residence_fields,
      {
        where: { id: residence_id }
      }
    ).then(() => {
      return true;
    });
  }

}

module.exports = ResidenceService;
