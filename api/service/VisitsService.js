const Visit = require('../models').visit;
const User = require('../models').user;
const VisitTouchpoint = require('../models').visit_touchpoint;

class VisitsService {
  
  getVisitsByResidenceId(residence_id) {
    return Visit.findAll({
      attributes: [
        'answer',
        'engagement_time',
        'resident_name',
        'createdAt',
        'user.forename',
        'user.surname'
      ],
      where: {
        residence_id
      },
      include: [
        {
          model: User,
          required: false
        }
      ],
      order: [['createdAt', 'DESC']]
    })
    .then(data => {
      return data;
    });
  }

  createNewVisit(residence_id, fields) {
    fields.residence_id = residence_id;
    return Visit.create(fields).then(data => {
      if (fields.touchpoints) {
        return this.createNewVisitTouchpoint(data.id, fields.touchpoints);
      }
      return data
    }).then(data => {
      return data
    })
  }

  createNewVisitTouchpoint(visit_id, touchpoint_ids) {
    if (touchpoint_ids === []) {return;}
    const touchpoints = touchpoint_ids.map((touchpoint_id) => {
      return {
        visit_id,
        touchpoint_id
      }
    });

    return VisitTouchpoint.bulkCreate(touchpoints)
      .then(() => {return true});
  }
}

module.exports = VisitsService;
