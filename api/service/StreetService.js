const Street = require('../models').street;
const Residence = require('../models').residence;
const Sequelize = require("sequelize");

class StreetService {
  createStreet(church_id, { name, postcode }) {
    return Street.create({
      church_id,
      name,
      postcode
    }).then(data => {
      return data.get();
    });
  }

  getStreets(church_id) {
    return Street.findAll({
      attributes: [ ['id', 'street_id'], 'name', 'postcode', [Sequelize.fn('COUNT', Sequelize.col('residences.id')), 'no_of_residences']],
      where: {
        church_id: church_id
      },
      include: [
        {
          model: Residence,
          required: false
        }
      ],
      group: ['street.id'],
      order: [['name', 'ASC']]
    }).then((data) => {
      return data;
    });
  }

  getStreetById(street_id) {
    return Street.findByPk(street_id)
    .then((data) => {
      return data.get();
    });
  }
}

module.exports = StreetService;
