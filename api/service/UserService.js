const User = require('../models').user;

class UserService {

  getUser(user_id) {
    return User.findOne({
      attributes: ['id', 'church_id', 'forename', 'surname', 'email', 'status'],
      where: {
        id: user_id
      }
    }).then((user) => {
      if (user) {
        const userInfo = user.get();
        return userInfo;
      } else {
        console.log('squark');
      }
    })
  }
  
}

module.exports = UserService;
