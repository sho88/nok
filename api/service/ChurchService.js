const Church = require('../models').church;

class ChurchService { 
  allChurches() {
    return Church.findAll()
    .then((church) => {
      return church;
    });
  }
  
  viewByChurchId(church_id) {
    return Church.findByPk(church_id)
    .then((church) => {
      return church.get();
    });
  }
}

module.exports = ChurchService;
