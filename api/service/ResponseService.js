const httpStatusCodes = require('http-status-codes');
const { isNull } = require('lodash');

class ResponseService {

  parse(res, data, status = httpStatusCodes.OK) {
    let success = true;
    
    if (status >= httpStatusCodes.BAD_REQUEST) {
      success = false;
    }

    return res.status(status).json({ success, data });
  }

}

module.exports = ResponseService;
