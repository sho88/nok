module.exports = function (sequelize, Sequelize) {

  const Touchpoint = sequelize.define(
    'touchpoint',
    {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      church_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'church',
          key: 'id',
        }
      },
      touchpoint_name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      status: {
        type: Sequelize.ENUM('active', 'inactive'),
        defaultValue: 'active'
      }
    }, {
      timestamps: true,
      freezeTableName: true,
    });

  return Touchpoint;
}
