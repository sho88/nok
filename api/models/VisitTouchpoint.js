module.exports = function (sequelize, Sequelize) {

  const VisitTouchpoint = sequelize.define(
    'visit_touchpoint',
    {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      visit_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'visit',
          key: 'id',
        }
      },
      touchpoint_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'touchpoint',
          key: 'id',
        }
      }
    }, {
      timestamps: false,
      freezeTableName: true,
    });

  return VisitTouchpoint;
}
