module.exports = function (sequelize, Sequelize) {
  const Church = sequelize.define(
    'church',
    {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      postcode: {
        type: Sequelize.STRING,
        allowNull: false,
      },

      status: {
        type: Sequelize.ENUM('active', 'inactive'),
        defaultValue: 'active'
      }
    }, {
      timestamps: true,
      freezeTableName: true,
    });

    Church.associate = function(models) {
      models.church.hasMany(models.user, { foreignKey: 'church_id' });
      models.church.hasMany(models.street, { onDelete: 'CASCADE', foreignKey: 'church_id' });
    }

  return Church;
}
