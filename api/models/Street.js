module.exports = function (sequelize, Sequelize) {

  const Street = sequelize.define(
    'street',
    {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      church_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'church',
          key: 'id',
        }
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      postcode: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      status: {
        type: Sequelize.ENUM('active', 'inactive'),
        defaultValue: 'active'
      }
    }, {
      timestamps: true,
      freezeTableName: true,
    }
  );

  Street.associate = function(models) {
    models.street.hasMany(models.residence, { onDelete: 'CASCADE', sourceKey: 'id', foreignKey: 'street_id' });
  }

  return Street;
}
