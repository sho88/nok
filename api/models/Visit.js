module.exports = function (sequelize, Sequelize) {

  const Visit = sequelize.define(
    'visit',
    {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      user_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'user',
          key: 'id',
        }
      },
      residence_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'residence',
          key: 'id',
        }
      },
      answer: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
      },
      engagement_time: {
        type: Sequelize.INTEGER,
        allowNull: false,
      },
      resident_name: {
        type: Sequelize.STRING
      }
    }, {
      getterMethods: {
        engagement: function() {
          let t;
          switch(this.getDataValue('engagement_time')) {
            case 0: t = "Less than a minute"; break;
            case 1: t = "Less than five minutes"; break;
            case 2: t = "Between 5 and 10 minutes"; break;
            case 3: t = "About 15 minutes"; break;
            case 4: t = "Longer than 15 minutes"; break;
            default: t = 'No record'; break;
          }
          return t;
        }
      },
      timestamps: true,
      freezeTableName: true,
    });

    Visit.associate = function(models) {
      models.visit.belongsTo(models.user, {source_key: 'user.id', foreignKey: 'user_id' });
      models.visit.belongsTo(models.residence, {source_key: 'residence.id', foreignKey: 'residence_id' });
      models.visit.hasMany(models.visit_touchpoint, { onDelete: 'CASCADE', foreignKey: 'visit_id'} );
    }

  return Visit;
}
