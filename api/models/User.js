const Church = require('../models').church;
module.exports = function (sequelize, Sequelize) {

  const User = sequelize.define(
    'user',
    {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      church_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'church',
          key: 'id',
        }
      },
      last_login: {
        type: Sequelize.DATE
      },
      forename: {
        type: Sequelize.STRING
      },
      surname: {
        type: Sequelize.STRING
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
        validate: {
          isEmail: true
        }
      },
      pass: {
        type: Sequelize.STRING,
        allowNull: false
      },
      status: {
        type: Sequelize.ENUM('active', 'inactive'),
        allowNull: false,
        defaultValue: 'active'
      }
  }, {
    getterMethods: {
      fullName: function () {
        return this.getDataValue('forename') + ' ' + this.getDataValue('surname');
      }
    },
    timestamps: true,
    freezeTableName: true,
  });

  User.associate = function(models) {
    models.user.hasMany(models.visit, { onDelete: 'CASCADE', foreignKey: 'user_id'} );
  }
  
  return User;
}
