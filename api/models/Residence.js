module.exports = function (sequelize, Sequelize) {

  const Residence = sequelize.define(
    'residence',
    {
      id: {
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
      },
      street_id: {
        type: Sequelize.INTEGER,
        references: {
          model: 'street',
          key: 'id',
        }
      },
      house_number: {
        type: Sequelize.STRING,
        allowNull: false,
      },
      do_not_knock: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
      },
      do_not_knock_date: {
        type: Sequelize.DATE,
        allowNull: true,
      },
      status: {
        type: Sequelize.ENUM('active', 'inactive'),
        defaultValue: 'active'
      }
    }, {
      timestamps: true,
      freezeTableName: true
    });

    Residence.associate = function(models) {
      models.residence.belongsTo(models.street, {source_key: 'street.id', foreignKey: 'street_id'});
      models.residence.hasMany(models.visit, { onDelete: 'CASCADE', foreignKey: 'residence_id'} );
    }

  return Residence;
}


