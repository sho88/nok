const Controllers = require('./controller');

const isLoggedIn = (req, res, next) => {
	if (!req.isAuthenticated()) {
		res.status(401).send('You are not authenticated');
	} else {
		return next();
	}
}

module.exports = (app, passport) => {
	app.get('/api', Controllers.IndexController.indexAction)

	app.post('/api/login', Controllers.IndexController.loginAction);
	app.get('/api/logout', Controllers.IndexController.logoutAction);

	// Current user
  app.get('/api/user', isLoggedIn, Controllers.IndexController.currentUserAction);
  app.post('/api/user', Controllers.IndexController.newUserAction);

	app.get('/api/street', isLoggedIn, Controllers.StreetController.indexAction)
	app.post('/api/street', isLoggedIn, Controllers.StreetController.indexAction)
	app.get('/api/street/:id', isLoggedIn, Controllers.StreetController.viewAction)
	app.get('/api/street/:id/residence', isLoggedIn, Controllers.ResidenceController.viewByStreetId)

	// Residence
	app.post('/api/residence', isLoggedIn, Controllers.ResidenceController.indexAction)
	app.patch('/api/residence', isLoggedIn, Controllers.ResidenceController.indexAction)
	app.get('/api/residence/:residenceId', isLoggedIn, Controllers.ResidenceController.viewByResidenceId)
	
	// Touchpoints
	app.get('/api/touchpoints', isLoggedIn, Controllers.TouchpointController.indexAction)
	app.post('/api/touchpoints', isLoggedIn, Controllers.TouchpointController.createAction)
	
	// Visits
	app.get('/api/visit', isLoggedIn, Controllers.VisitsController.indexAction)
	app.post('/api/visit', isLoggedIn, Controllers.VisitsController.indexAction)

	// Church
	app.get('/api/church', isLoggedIn, Controllers.ChurchController.indexAction)
	app.get('/api/church/:church_id', isLoggedIn, Controllers.ChurchController.viewByChurchId)
	// app.post('/api/church', isLoggedIn, churchAction);

}
